#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <unistd.h>

using namespace std;
using namespace cv;
String haarcascade = "rasia.xml";

void HandDetection(Mat frame);


int main(){
	
	VideoCapture cap(0);
	CascadeClassifier hand_cascade;
	Mat frame, frame_gray;
	vector<Rect> hand;
	Mat frame_pracujacy;
	
	int gl = 0;
	int gp = 0;
	int dl = 0;
	int dp = 0;
	
	if(!hand_cascade.load( haarcascade )){
		cout << " Nie mogę wczytać pliku .xml " << endl ;
		return -1;
	}
	 if(!cap.isOpened()) {
		cout << " Nie mogę odpalić kamerki " << endl; 
        return -1;	
	}
	int ttl = 0 ;
	while(true){
	 		
		cap >> frame;
	    cvtColor(frame, frame_gray, CV_BGR2GRAY);
		equalizeHist(frame_gray, frame_gray);
		imshow ("equalizeHist", frame_gray);


		hand_cascade.detectMultiScale(frame, hand, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(100,100));
		if (ttl >0) ttl --;
		else	{
	  for( size_t i = 0; i < hand.size(); i++ ){
		  ttl = 10;	
		  Point center ( hand[i].x + hand[i].width* 0.5, hand[i].y + hand[i].height*0.5);
		  ellipse (frame, center,Size(hand[i].width*0.5, hand[i].height*0.5), 0,0, 360, Scalar( 255,255,255), 4,8,0);
		
		int x = hand[i].x ;
		int y = hand[i].y ;

		cout << " x = "  << x << " y = "  << y <<endl;
		if ( x > 400 && y < 80)
			gp = 1;
			cout << "ustawiłem gorny prawy na 1. GP = " << gp << endl;
		
		if ( x < 70 && y < 80)
			gl = 1;
			cout << " ustawilem gory lewy na 1. GL = " <<gl << endl;
			
		if (x < 50 && y > 250 && y < 350)
			dl = 1;
			cout << " ustawiam dolny lewy na 1. DL = "<< dl << endl;
		if (x  > 450 && y  > 250  )
			dp = 1;
			cout << "ustawiam dolny prawy na 1. DP = "<< dp << endl;
			
		if ( gp == 1 ){
			if(gl ==1){
			cout << " gp = " << gp <<endl;
			cout << "gl =  " << gl << endl ;
			system("firefox");
			gp =0;
			gl =0;
			cout << " Zeruje gp i gl " << endl;
			cout << " gp = " << gp <<endl;
			cout << " gl = " << gl <<endl;
	}}
	
	
		if ( dl == 1 ){
			if(dp ==1){
			cout << " gp = " << gp <<endl;
			cout << "gl =  " << gl << endl ;
			system("eog -f medytacja.jpg");
			dl =0;
			dp =0;
			cout << " Zeruje gp i gl " << endl;
			cout << " gl = " << gp <<endl;
			cout << " gp = " << gl <<endl;
		}}
		
		
		if ( dl == 1 ){
			if(gp ==1){
			cout << " gp = " << gp <<endl;
			cout << "gl =  " << gl << endl ;
			system("eog -f da.jpg");
			gl =0;
			gp =0;
			cout << " Zeruje gp i gl " << endl;
			cout << " gl = " << gp <<endl;
			cout << " gp = " << gl <<endl;
		}}
		
		if ( dp == 1 ){
			if(gl ==1){
			cout << " gp = " << gp <<endl;
			cout << "gl =  " << gl << endl ;
			system("libreoffice --writer");
			dp =0;
			gl =0;
			cout << " Zeruje gp i gl " << endl;
			cout << " gl = " << gp <<endl;
			cout << " gp = " << gl <<endl;
		}}
		
	
	
		/* if(x > 420  && y < 100){`
			system("firefox"); // góraprawy;
			cout << "uruchamiam";
			cout << x << y;
			break;
			}
				
		
		if(x < 100 && y <= 50){
						
			system ("libreoffice --writer "); // góra lewy	
			cout << "uruchamiam";`
			cout << x << y;
			x = 0;
			y = 0;			break;

		}
		
		
		if(x < 50 && y > 300){
				
			system("eog -f da.jpg"); // dół lewy
			cout << "uruchamiam";
			cout << x << y;
			x = 0;
			y = 0;				break;

	    }
	    	
	    if(x > 450 && y > 300){
					
			system("eog -f medytacja.jpg"); // dół lewy
			cout << "uruchamiam";
			cout <<  " x = " << x << "y = " << y ;
			x = 0;
			y = 0;				break; */

		}
	
	}
		

	   circle(frame, Point(600,30) , 20, Scalar(0,0,0), 2, 8, 0 ); // góra prawe
	   circle(frame, Point(30,30) , 20, Scalar(0,0,0), 2, 8, 0 ); // góra lewe
	   circle(frame, Point(30, 450) , 20, Scalar(0,0,0), 2, 8, 0 ); //dół lewy
	   circle(frame, Point(600,450) , 20, Scalar(0,0,0), 2, 8, 0 ); //dół prawy
	  imshow("asdasd", frame);
	//  imshow("Reka", frame_pracujacy);
 
        if( (waitKey(30) == 27 ) ){
			cout << " No elo " << endl;
			 break;
			  }
	}
	return 0;
}
